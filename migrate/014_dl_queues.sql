create table dl_jobs (
    id serial primary key, 
    url text not null, 
    messsages text not null default '', 
    created timestamp not null default now(), 
    status text not null default 'not_started',
    progress real not null default 0);
alter table dl_fail_queue add column job_id integer references dl_jobs(id);
alter table dl_fail_queue add column message text;