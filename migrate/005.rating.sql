create type e_rating as enum ('useful', 'neutral', 'useless');
alter table videos add column rating e_rating not null default 'neutral';