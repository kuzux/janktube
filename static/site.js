class Video {
    constructor(element) {
        this._elem = element;
    }

    seekTo(fnOrPosition) {
        const media = this._elem;
        const position = (fnOrPosition.call) ? fnOrPosition(media.currentTime) : fnOrPosition;
        media.currentTime = _.clamp(position, 0, media.duration);
    }

    changeVolume(fnOrValue) {
        const media = this._elem;
        const value = (fnOrValue.call) ? fnOrValue(media.volume) : fnOrValue;
        media.volume = _.clamp(value, 0.0, 1.0);
    }

    changeSpeed(fnOrValue) {
        const media = this._elem;
        const value = (fnOrValue.call) ? fnOrValue(media.playbackRate) : fnOrValue;
        media.playbackRate = _.clamp(value, 0.1, 3.0);
    }

    toggleMute() {
        this._elem.muted = !this._elem.muted;
    }

    togglePlaying() {
        if (this._elem.paused) {
            this._elem.play();
        } else {
            this._elem.pause();
        }
    }

    toggleFullscreen() {
        if (document.fullscreen) {
            document.exitFullscreen();
        } else {
            this._elem.requestFullscreen();
        }
    }

    addTimeListener(cb) {
        this._elem.addEventListener("timeupdate", () => cb(this._elem.currentTime, this._elem.duration));
        if (this._elem.readyState >= 2) {
            cb(this._elem.currentTime, this._elem.duration)
        } else {
            this._elem.addEventListener("loadeddata", () => cb(this._elem.currentTime, this._elem.duration));
        }
    }

    addSpeedListener(cb) {
        this._elem.addEventListener("ratechange", () => cb(this._elem.playbackRate));
        cb(this._elem.playbackRate);
    }

    addLoadedListener(cb) {
        if (this._elem.readyState >= 2) {
            cb()
        }

        this._elem.addEventListener("loadeddata", () => cb());
    }

    addEndedListener(cb) {
        if (this._elem.ended) {
            cb()
        }

        this._elem.addEventListener("ended", () => cb());
    }

    paused() {
        return this._elem.paused;
    }

    currentTime() {
        return this._elem.currentTime;
    }

    totalDuration() {
        return this._elem.duration;
    }

    elapsedRatio() {
        return this._elem.currentTime / this._elem.duration;
    }

    secsFromRatio(ratio) {
        return this._elem.duration * ratio;
    }

    volume() {
        return this._elem.volume;
    }

    muted() {
        return this._elem.muted;
    }

    fullscreen() {
        return document.fullscreen;
    }

    speed() {
        return this._elem.playbackRate;
    }

    rememberSize() {
        this._oldWidth = this._elem.width;
        this._oldHeight = this._elem.height;
    }

    restoreSize() {
        if (!this._oldWidth || !this._oldHeight) {
            throw "Can't restore to old size. No old size to restore to";
        }

        this._elem.width = this._oldWidth;
        this._elem.height = this._oldHeight;
    }

    goFullscreen() {
        this._elem.width = window.outerWidth;
        this._elem.height = window.outerHeight;
    }

    resizeElementToFit(element) {
        element.width = this._elem.width;
        element.height = this._elem.height;
    }

    static toMinuteSecond(rawSeconds) {
        const intValue = Math.round(rawSeconds);

        const minutes = Math.floor(intValue / 60);
        const seconds = Math.floor(intValue % 60);

        const minuteStr = minutes.toString().padStart(2, '0');
        const secondStr = seconds.toString().padStart(2, '0');

        return `${minuteStr}:${secondStr}`
    }

    elapsedTimeText() {
        const currTimeText = Video.toMinuteSecond(this._elem.currentTime);
        const totalTimeText = Video.toMinuteSecond(this._elem.duration);

        return `${currTimeText}/${totalTimeText}`;
    }

    remainingTimeText() {
        const remTimeText = Video.toMinuteSecond(this._elem.duration - this._elem.currentTime);
        const totalTimeText = Video.toMinuteSecond(this._elem.duration);

        return `-${remTimeText}/${totalTimeText}`;
    }

    hasSubtitles() {
        return this._elem.textTracks.length > 0;
    }

    subtitlesEnabled() {
        const track = this._elem.textTracks[0];
        return track && track.mode == "showing";
    }

    toggleSubtitles() {
        if (!this.hasSubtitles()) return;

        if (this.subtitlesEnabled()) {
            this._elem.textTracks[0].mode = "hidden";
        } else {
            this._elem.textTracks[0].mode = "showing";
        }
    }
};

class Rect {
    constructor(x, y, w, h) {
        this.x = x;
        this.y = y;
        this.w = w;
        this.h = h;
    }

    drawTo(ctx) {
        ctx.fillRect(this.x, this.y, this.w, this.h);
    }

    contains(arg) {
        if (!arg) return false;
        return _.inRange(arg[0], this.x, this.x + this.w) &&
            _.inRange(arg[1], this.y, this.y + this.h);
    }

    static empty() {
        return new Rect(0, 0, 0, 0);
    }
};

class Icon {
    constructor(active, inactive) {
        this.active = active;
        this.inactive = inactive;
    }

    drawActiveTo(rect, ctx) {
        ctx.drawImage(this.active, rect.x, rect.y,
            rect.w, rect.h);
    }

    drawInactiveTo(rect, ctx) {
        ctx.drawImage(this.inactive, rect.x, rect.y,
            rect.w, rect.h)
    }

    static _createIcon(name) {
        const activeIcon = new Image();
        activeIcon.src = `/static/icons/${name}_active.svg`;
        const inactiveIcon = new Image();
        inactiveIcon.src = `/static/icons/${name}_inactive.svg`;

        return new Icon(activeIcon, inactiveIcon)
    }

    static loadIcons() {
        const iconNames = ["play", "pause", "mute", "volume_low", "volume_mid",
            "volume_hi", "fullscreen", "no_fullscreen", "underline"];
        const pairs = _.map(iconNames, name => [name, Icon._createIcon(name)])
        window.icons = _.fromPairs(pairs);
    }

    static getByName(name) {
        return window.icons[name];
    }
};

class Button {
    constructor(rect, iconCb) {
        this.rect = rect;
        this.iconCb = iconCb;
        this.clickCb = () => { };
    }

    isActive(hoverPos) {
        return this.rect.contains(hoverPos);
    }

    onClick(cb) {
        this.clickCb = cb;
    }

    drawActiveIconTo(ctx) {
        this.iconCb().drawActiveTo(this.rect, ctx);
    }

    drawInactiveIconTo(ctx) {
        this.iconCb().drawInactiveTo(this.rect, ctx);
    }

    drawCurrentIconTo(hoverPos, ctx) {
        if (this.isActive(hoverPos))
            this.drawActiveIconTo(ctx);
        else
            this.drawInactiveIconTo(ctx);
    }

    moveAndResizeTo(x, y, w, h) {
        this.rect = new Rect(x, y, w, h);
    }
};

class ClickableArea {
    constructor(rect, cb) {
        this.rect = rect;
        this.clickCb = cb;
    }

    isActive(pos) {
        return this.rect.contains(pos);
    }

    moveAndResizeTo(x, y, w, h) {
        this.rect = new Rect(x, y, w, h);
    }
}

class Thumbnail {
    constructor(time, url) {
        this.time = time;
        this.image = new Image();
        this.image.src = url;
    }

    static findMatchingImage(secs) {
        if (window.previewImages.length == 0) return null;

        var matchingThumbnail = _.find(window.previewImages, (data) => data.time >= secs);
        if (!matchingThumbnail) matchingThumbnail = _.last(window.previewImages);

        return matchingThumbnail.image;
    }

    static loadPreviewImages = (vid) => {
        const thumbnailReq = new Request(`/thumbs/${vid}.json`);
        fetch(thumbnailReq).then((resp) => {
            if (resp.ok) {
                resp.json().then((data) => {
                    window.previewImages = _.map(data, (sec) => {
                        const url = `/thumbs/${vid}/${sec}.jpg`;
                        return new Thumbnail(sec, url);
                    });
                });
            }
        });
    };
}

class Controls {
    constructor(container) {
        this._container = container;
        this._video = new Video(container.querySelector("video"));
        this._canvas = container.querySelector("canvas");

        this.lastMouseMoveTime = 0;
        this.showRemaining = false;

        this.widgets = [];
        this.clickCbs = [];
        this.bgClickCb = () => { };
    }

    toggleFullscreen() {
        if (document.fullscreen) {
            document.exitFullscreen();
        } else {
            this._container.requestFullscreen();
        }
    }

    startDisplay() {
        this._video.addLoadedListener(() => this._startDisplay());
    }

    _startDisplay() {
        this._video.resizeElementToFit(this._canvas);

        this.playPauseButton = new Button(Rect.empty(), () =>
            Icon.getByName(this._video.paused() ? 'play' : 'pause'));
        this.muteButton = new Button(Rect.empty(), () => {
            if (this._video.muted()) {
                if (this._video.volume < 0.33) return Icon.getByName("volume_low");
                else if (this._video.volume < 0.66) return Icon.getByName("volume_mid");
                else return Icon.getByName("volume_hi");
            } else {
                return Icon.getByName("mute");
            }
        });
        this.fullscreenButton = new Button(Rect.empty(), () =>
            Icon.getByName(this._video.fullscreen() ? 'no_fullscreen' : 'fullscreen'));
        this.subtitleButton = new Button(this.underlineButtonRect, () =>
            Icon.getByName('underline'));

        this.playPauseButton.onClick(() => this._video.togglePlaying());
        this.addWidget(this.playPauseButton);

        this.muteButton.onClick(() => this._video.toggleMute());
        this.addWidget(this.muteButton);

        this.subtitleButton.onClick(() => this._video.toggleSubtitles());
        this.addWidget(this.subtitleButton);

        this.fullscreenButton.onClick(() => this.toggleFullscreen());
        this.addWidget(this.fullscreenButton);

        this.seekbar = new ClickableArea(Rect.empty(), (rect, [clickx, _clicky]) => {
            const newTime = this._video.secsFromRatio(clickx / rect.w);
            this._video.seekTo(newTime);
        });
        this.addWidget(this.seekbar);

        this.volumebar = new ClickableArea(Rect.empty(), (rect, [clickx, _clicky]) => {
            const newVolume = clickx / rect.w;
            this._video.changeVolume(newVolume);
        });
        this.addWidget(this.volumebar);

        this.remainingTime = new ClickableArea(Rect.empty(), () => this.showRemaining = !this.showRemaining);
        this.addWidget(this.remainingTime);

        this.recalculateWidgetSizes();

        this.bgClickCb = () => this._video.togglePlaying();
        this._canvas.addEventListener('dblclick', () => this.toggleFullscreen());

        this._canvas.addEventListener('mousemove', (evt) => {
            this.lastMouseMoveTime = evt.timeStamp;
            this.lastHoverLocation = [evt.offsetX, evt.offsetY];
        });

        this._canvas.addEventListener('click', (evt) => {
            const pairs = _.filter(this.widgets, (widget) => widget.isActive([evt.offsetX, evt.offsetY]));

            if (pairs.length == 0) {
                this.bgClickCb([evt.offsetX, evt.offsetY]);
            }

            _.forEach(pairs, widget => {
                const localOffset = [evt.offsetX - widget.rect.x, evt.offsetY - widget.rect.y];
                widget.clickCb(widget.rect, localOffset);
            });
        });

        document.addEventListener("fullscreenchange", (evt) => {
            if (document.fullscreen) {
                this._video.rememberSize();
                this._video.goFullscreen();
                this._video.resizeElementToFit(this._canvas);
            } else {
                this._video.restoreSize();
                this._video.resizeElementToFit(this._canvas);
            }
        });

        window.requestAnimationFrame((timestamp) => this.drawFrame(timestamp));
    }

    // TODO: Move the layout calculation logic to its own class
    recalculateWidgetSizes() {
        const width = this._canvas.width;
        const height = this._canvas.height;

        // creating the layout
        // spacer -- play/pause -- spacer -- seekbar -- spacer -- mute -- volume bar -- spacer -- underline -- spacer -- fullscreen
        const barHeight = 30;
        const buttonWidth = 30;

        const spacerWidth = 8;

        // I need to do this to mutate the old rect objects that are already in the cb array
        // (I can do this immutably, by recomputing the cbs on size change, but it was more work)
        // TODO: do it that way
        this.playPauseButton.moveAndResizeTo(spacerWidth, height - barHeight, buttonWidth, barHeight);

        const seekbarWidth = (width - 4 * buttonWidth - 5 * spacerWidth) * 0.85;
        this.seekbar.moveAndResizeTo(buttonWidth + 2 * spacerWidth, height - barHeight, seekbarWidth, barHeight);

        this.muteButton.moveAndResizeTo(buttonWidth + seekbarWidth + 3 * spacerWidth, height - barHeight, buttonWidth, barHeight);

        const volumebarWidth = (width - 4 * buttonWidth - 5 * spacerWidth) * 0.15;
        this.volumebar.moveAndResizeTo(2 * buttonWidth + 3 * spacerWidth + seekbarWidth, height - barHeight, volumebarWidth, barHeight);

        this.subtitleButton.moveAndResizeTo(width - 2 * buttonWidth - spacerWidth, height - barHeight, buttonWidth, barHeight);
        this.fullscreenButton.moveAndResizeTo(width - buttonWidth, height - barHeight, buttonWidth, barHeight);

        // we change this guy's size in the draw method
        // also it is the upper left corner of the text
        this.remainingTime.moveAndResizeTo(20, height - barHeight - 30, this.remainingTime.rect.w, this.remainingTime.rect.h);
    }

    drawFrame(timestamp) {
        const width = this._canvas.width;
        const height = this._canvas.height;

        this.recalculateWidgetSizes();

        const timeSinceMouseMove = timestamp - this.lastMouseMoveTime

        const ctx = this._canvas.getContext('2d');
        ctx.clearRect(0, 0, width, height);

        if (timeSinceMouseMove <= 2500) {
            this.drawFullGradients(ctx);
            this.drawControls(ctx);
            this.drawVideoTitle(ctx);
            this.drawElapsedTime(ctx);
            if (this.seekbar.rect.contains(this.lastHoverLocation)) {
                this.drawHoverPreview(ctx);
            }
        }

        window.requestAnimationFrame((timestamp) => this.drawFrame(timestamp));
    }

    drawFullGradients(ctx) {
        const width = this._canvas.width;
        const height = this._canvas.height;

        // I don't need to recalculate the gradient on every frame but it is not a big deal
        const grd = ctx.createLinearGradient(0, 0, 0, height);
        grd.addColorStop(0, "rgba(0, 0, 0, 0.3)");
        grd.addColorStop(0.35, "rgba(0, 0, 0, 0)");
        grd.addColorStop(0.65, "rgba(0, 0, 0, 0)");
        grd.addColorStop(1, "rgba(0, 0, 0, 0.3)");

        ctx.fillStyle = grd;
        ctx.fillRect(0, 0, width, height);
    }

    partialSeekbarRect() {
        const partialWidth = this._video.elapsedRatio() * this.seekbar.rect.w;
        return new Rect(this.seekbar.rect.x, this.seekbar.rect.y, partialWidth, this.seekbar.rect.h);
    }

    partialVolumeRect() {
        const partialWidth = this._video.volume() * this.volumebar.rect.w;
        return new Rect(this.volumebar.rect.x, this.volumebar.rect.y, partialWidth, this.volumebar.rect.h);
    }

    drawVolumeDisplay(rect, ctx) {
        const partialW = this._video.volume() * rect.w;
        const partialH = this._video.volume() * rect.h;

        ctx.beginPath();
        ctx.moveTo(rect.x, rect.y + rect.h);
        ctx.lineTo(rect.x + rect.w, rect.y);
        ctx.lineTo(rect.x + rect.w, rect.y + rect.h);
        ctx.closePath();

        ctx.fillStyle = 'rgba(255, 0, 255, 0.3)';
        ctx.fill();

        ctx.beginPath();
        ctx.moveTo(rect.x, rect.y + rect.h);
        ctx.lineTo(rect.x + partialW, rect.y + rect.h - partialH);
        ctx.lineTo(rect.x + partialW, rect.y + rect.h);
        ctx.closePath();

        ctx.fillStyle = 'rgba(255, 0, 255, 0.6)';
        ctx.fill();
    }

    drawControls(ctx) {
        this.playPauseButton.drawCurrentIconTo(this.lastHoverLocation, ctx);

        ctx.fillStyle = 'rgba(255, 255, 0, 0.3)';
        this.seekbar.rect.drawTo(ctx);
        const partialSeekbarRect = this.partialSeekbarRect();
        ctx.fillStyle = 'rgba(255, 255, 0, 0.6)';
        partialSeekbarRect.drawTo(ctx);

        this.muteButton.drawCurrentIconTo(this.lastHoverLocation, ctx);
        this.drawVolumeDisplay(this.volumebar.rect, ctx);

        this.subtitleButton.drawCurrentIconTo(this.lastHoverLocation, ctx);
        this.fullscreenButton.drawCurrentIconTo(this.lastHoverLocation, ctx);
    }

    addWidget(child) {
        this.widgets.push(child);
    }

    drawVideoTitle(ctx) {
        // no need to scale these things, really
        // we specify the lower left corner, so y = x + font size
        const titleX = 20;
        const titleY = 50;

        ctx.font = "bold 30px sans-serif";
        ctx.fillStyle = "rgba(255, 255, 255, 1.0)";
        ctx.strokeStyle = "rgba(0, 0, 0, 1.0)";

        ctx.fillText(window.videoTitle, titleX, titleY);
        ctx.strokeText(window.videoTitle, titleX, titleY);
    }

    drawElapsedTime(ctx) {
        const rect = this.remainingTime.rect;

        ctx.font = "bold 20px sans-serif";
        ctx.fillStyle = "rgba(255, 255, 255, 1.0)";
        ctx.strokeStyle = "rgba(0, 0, 0, 1.0)";

        const text = (this.showRemaining) ?
            this._video.remainingTimeText() :
            this._video.elapsedTimeText();
        const textWidth = ctx.measureText(text).width;

        this.remainingTime.moveAndResizeTo(rect.x, rect.y, textWidth, 20);

        // specify the lower left coords
        ctx.fillText(text, rect.x, rect.y + 20);
        ctx.strokeText(text, rect.x, rect.y + 20);
    }

    drawHoverPreview(ctx) {
        const hoverX = this.lastHoverLocation[0];
        const seekbarPosPx = hoverX - this.seekbar.rect.x;
        const hoverPreviewRatio = seekbarPosPx / this.seekbar.rect.w;

        const secs = this._video.secsFromRatio(hoverPreviewRatio);
        const text = Video.toMinuteSecond(secs);

        // layout:
        // 5px top margin
        // 5px left margin - img.width wide, img.height high thumb - 5px right margin
        // 5px mid margin
        // calculated left margin - 14px high, calculated wide centered text - calculated right margin
        // 7px bottom margin
        // 8px empty margin (and a triangle)
        // bottom bar
        // the preview thing is centered around the hovering pixel
        ctx.font = "14px sans-serif";

        // TODO: Handle the case wher the box runs out of the player
        const centerX = this.seekbar.rect.x + seekbarPosPx;
        const bottomLineY = this.seekbar.rect.y;

        // TODO: Handle the case where image is not available (is null)
        const img = Thumbnail.findMatchingImage(secs);

        const outerBoxWidth = 5 + img.width + 5;
        const outerBoxHeight = 5 + img.height + 5 + 14 + 7;
        const outerBoxX = centerX - (outerBoxWidth / 2);
        const outerBoxY = bottomLineY - 8 - outerBoxHeight;

        const thumbX = outerBoxX + 5;
        const thumbY = outerBoxY + 5;

        const textWidth = ctx.measureText(text).width;

        const textLeftMargin = (outerBoxWidth - textWidth) / 2;
        const textX = outerBoxX + textLeftMargin;
        const textY = outerBoxY + 5 + img.height + 5 + 14;

        ctx.fillStyle = "rgba(0, 0, 0, 1.0)";
        ctx.fillRect(outerBoxX, outerBoxY, outerBoxWidth, outerBoxHeight);

        const diffLen = 8 * Math.sqrt(3) / 2;

        // drawing a small triangle at the center point
        ctx.beginPath();

        ctx.moveTo(centerX - diffLen, outerBoxY + outerBoxHeight);
        ctx.lineTo(centerX, bottomLineY);
        ctx.lineTo(centerX + diffLen, outerBoxY + outerBoxHeight);

        ctx.closePath();
        ctx.fill();

        ctx.drawImage(img, thumbX, thumbY);

        ctx.fillStyle = "rgba(255, 255, 255, 1.0)";
        ctx.fillText(text, textX, textY);
    }
};

const updatePlaybackSpeedDisplay = (newRate) => {
    const display = document.querySelector(".video-controls .current-speed");

    const pct = Math.round(100.0 * newRate);
    display.innerText = `Current speed: ${pct}%`;
};

const loadPlaybackSpeedDisplay = (media) => {
    const fasterButton = document.querySelector(".video-controls .faster");
    const slowerButton = document.querySelector(".video-controls .slower");
    if (!fasterButton || !slowerButton) return;

    fasterButton.addEventListener("click", () => media.changeSpeed(oldSpd => oldSpd + 0.1));
    slowerButton.addEventListener("click", () => media.changeSpeed(oldSpd => oldSpd - 0.1));

    media.addSpeedListener(updatePlaybackSpeedDisplay);
};

const loadKeyboardShortcuts = (media) => {
    window.addEventListener("keydown", (evt) => {
        switch (evt.key) {
            case "Space":
            case " ":
                media.togglePlaying();
                break;
            case "ArrowLeft":
                if (evt.shiftKey) {
                    media.seekTo(oldPos => oldPos - 30);
                } else if (evt.ctrlKey) {
                    media.seekTo(oldPos => oldPos - 1);
                } else {
                    media.seekTo(oldPos => oldPos - 10);
                }
                break;
            case "ArrowRight":
                if (evt.shiftKey) {
                    media.seekTo(oldPos => oldPos + 30);
                } else if (evt.ctrlKey) {
                    media.seekTo(oldPos => oldPos + 1);
                } else {
                    media.seekTo(oldPos => oldPos + 10);
                }
                break;
            case "ArrowUp":
                media.changeVolume(oldVol => oldVol + 0.1);
                break;
            case "ArrowDown":
                media.changeVolume(oldVol => oldVol - 0.1);
                break;
            case "BracketLeft":
            case "[":
                media.changeSpeed(oldSpd => oldSpd - 0.1);
                break;
            case "BracketRight":
            case "]":
                media.changeSpeed(oldSpd => oldSpd + 0.1);
                break;
            case "m":
            case "M":
                media.toggleMute();
                break;
        }
    });
};

const setWatchedTimeUpdater = (media) => {
    // so that we do not spam the server with extra requests every time video timer is updated
    window.partialWatchInTransit = false;
    media.addTimeListener((curr, totalDuration) => {
        // Don't send any (partial or complete) requests if the video is already watched
        if (window.completed) return;

        const timediff = curr - window.lastWatchedTime;
        if (timediff >= 30 && !window.partialWatchInTransit) {
            // this also ignores if we go back and rewatch
            // but I'm not sure if there is a solution to this issue

            window.partialWatchInTransit = true;
            const seconds = Math.round(curr);
            const vid = window.vid;
            fetch(`/mark-partial/${vid}/${seconds}`, { method: 'POST' }).then(res => {
                if (res.ok) {
                    window.lastWatchedTime = seconds;
                } else {
                    console.log("Mark partial returned an error response: ");
                    console.log(res);
                }
                window.partialWatchInTransit = false;
            }).catch((err) => {
                console.log("Mark partial got an error: ");
                console.log(err);
                window.partialWatchInTransit = false;
            });
        }

        // TODO: this will spam the server until the response to the first request is received
        if (curr >= totalDuration * 0.95 && !window.completed) {
            fetch(`/mark-complete/${vid}`, { method: 'POST' }).then(res => {
                if (res.ok) {
                    window.completed = true;
                    window.lastWatchedTime = totalDuration;
                } else {
                    console.log("Mark complete returned an error response: ");
                    console.log(res);
                }
            }).catch((err) => {
                console.log("Mark complete got an error: ");
                console.log(err);
                // so that we can retry
                window.completed = false;
            });
        }
    });
};


// from https://stackoverflow.com/a/2117523/1936271
const uuidv4 = () => {
    return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
        (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
    );
};

const doHeartbeat = (socket, uuid, media) => {
    const payload = { id: uuid, curr_time: media.currentTime() };
    socket.emit("heartbeat", payload);
};

const connectToSocket = (media) => {
    const socket = io('http://localhost:5001/');
    const id = uuidv4();

    var heartbeatInterval = null;
    socket.on('connect', () => {
        media.addLoadedListener(() => {
            socket.emit('start', {
                id: id, vid: window.vid, title: window.videoTitle,
                duration: media.totalDuration(), curr_time: media.currentTime()
            });
            heartbeatInterval = window.setInterval(() => doHeartbeat(socket, id, media), 1000);
        });
    });

    socket.on('disconnect', () => {
        window.clearInterval(heartbeatInterval);
        heartbeatInterval = null;
    });

    return id;
};

const connectToControlSocket = (id, media) => {
    const listenSocket = io(`http://localhost:5001/player/${id}`);

    listenSocket.on('playpause', () => {
        media.togglePlaying();
    });

    listenSocket.on('fullscreen', () => {
        media.toggleFullscreen();
    });

    listenSocket.on('rewind', () => {
        media.seekTo(oldPos => oldPos - 10);
    });

    listenSocket.on('fastforward', () => {
        media.seekTo(oldPos => oldPos + 10);
    });

    listenSocket.on('mute', () => {
        media.toggleMute();
    });
};

const jumpToLastWatchedTime = (media) => {
    media.addLoadedListener(() => media.seekTo(window.lastWatchedTime));
};

const jumpToWatchNextOnEnd = (media) => {
    media.addEndedListener(() => {
        const watchNext = document.querySelector(".watch-next");

        if (document.fullscreen) {
            document.exitFullscreen();
            document.addEventListener("fullscreenchange", () => {
                watchNext.scrollIntoViewIfNeeded();
            });
        } else {
            watchNext.scrollIntoViewIfNeeded();
        }
    });
};

const closeFlash = () => {
    const flash = document.querySelector(".flash-messages");
    flash.hidden = true;
    const media = document.querySelector("video");
    if (!media) return;

    media.controls = true;
}

const loadFlashCloseButton = () => {
    const flash = document.querySelector(".flash-messages p");
    if (!flash) return;

    const elem = document.createElement("span");
    elem.classList.add("close-button");
    elem.innerHTML = "&#10005";
    elem.addEventListener("click", closeFlash)

    flash.appendChild(elem);
};

const listenForTaps = (elem) => {
    var tappedOnce = false;
    var tapTimeout = null;

    // TODO: Make sure we tapped in (roughly) the same place to register a double tap
    elem.addEventListener("touchstart", (evt) => {
        if (tappedOnce) {
            clearTimeout(tapTimeout);
            const event = new CustomEvent("dbltap", {
                detail: {},
                bubbles: true,
                cancelable: true
            });
            elem.dispatchEvent(event);
            tappedOnce = false;
        } else {
            tapTimeout = setTimeout(() => {
                const event = new CustomEvent("tap", {
                    detail: {},
                    bubbles: true,
                    cancelable: true
                });
                elem.dispatchEvent(event);
                tappedOnce = false;
            }, 300);
            tappedOnce = true;
        }
    });
}

const adjustMobileUi = (rawMedia) => {
    rawMedia.controls = true;
    const media = new Video(rawMedia);

    const elementsToHide = document.querySelectorAll(".desktop-only");
    _.forEach(elementsToHide, (node) => node.hidden = true);

    const overlay = document.querySelector(".video-overlay");
    listenForTaps(overlay);

    overlay.addEventListener("tap", (evt) => media.togglePlaying());
    // I tried way too many times to get touch events in fullscreen to work on
    // mobile (specifically, firefox on android, chrome has a workaround) and
    // it does not work (chrome's workaround is to make the overlay fullscreen
    // which has its own set of issues)
    overlay.addEventListener("dbltap", (evt) => media.toggleFullscreen());
};

const setupOverlay = (media) => {
    const overlay = document.querySelector(".video-overlay");
    media.resizeElementToFit(overlay);
}

const buildDesktopUi = (media) => {
    loadKeyboardShortcuts(media);
    jumpToWatchNextOnEnd(media);

    const container = document.querySelector(".video-container");
    const controls = new Controls(container);

    controls.startDisplay();
}

const hasVideoPlayer = () => (document.querySelector("video") != null);
const onMobile = () => /iphone|ipod|ipad|android|opera mini/i.test(navigator.userAgent.toLowerCase());

window.addEventListener("load", () => {
    if (hasVideoPlayer()) {
        const rawMedia = document.querySelector("video");
        const media = new Video(rawMedia);
        window.previewImages = null;
        Thumbnail.loadPreviewImages(window.vid);
        Icon.loadIcons();

        setupOverlay(media);

        if (onMobile(media)) {
            adjustMobileUi(rawMedia);
        } else {
            // TODO: Move the video ui elements out of the view html
            buildDesktopUi(media);
        }

        // we do these on both ui s
        loadPlaybackSpeedDisplay(media);
        jumpToLastWatchedTime(media);
        setWatchedTimeUpdater(media);

        // remote control stuff
        const playerId = connectToSocket(media);
        connectToControlSocket(playerId, media);

        // for debug purposes
        window.video = media;
        window.playerId = playerId;
    }

    loadFlashCloseButton();
});
