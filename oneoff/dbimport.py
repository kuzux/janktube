#!/usr/bin/env python3

import sqlite3
import psycopg2

connstr = "postgresql://arda:123456@localhost/vidsite"
olddb = "/media/arda/archive/youtube-vids/janktube/vids.db"

with sqlite3.connect(olddb) as sqlite, psycopg2.connect(connstr) as psql:
    sqlite.row_factory = sqlite3.Row

    oldcur = sqlite.cursor()
    newcur = psql.cursor()

    oldcur.execute("""select vid, name, presenter, event, created, watched, 
        filename, duration, group_concat(tag, ', ') as tags 
    from videos left join tags
        on tags.video_id = videos.vid
    group by vid, name, presenter, event, created, watched""")
    oldrows = oldcur.fetchall()

    newcur.executemany(
        """insert into videos (vid, name, presenter, event, added, watched, path, 
            duration, description) 
        values (%(vid)s, %(name)s, %(presenter)s, %(event)s, %(created)s, %(watched)s,
            %(filename)s, %(duration)s, %(tags)s)""", oldrows)
    psql.commit()
