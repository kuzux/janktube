create table dl_fail_queue (id serial primary key, url text, failures integer);
create table thumb_fail_queue (id serial primary key, data json, failures integer);