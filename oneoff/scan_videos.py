#!/usr/bin/env python3
import psycopg2
import glob
import os
import elasticsearch
from pymediainfo import MediaInfo


def find_duration(filename):
    info = MediaInfo.parse(filename)
    raw = next(filter(lambda t: t.track_type == 'Video', info.tracks)).duration
    return round(float(raw))//1000  # return the value in seconds


video_path = "/media/arda/archive/youtube-vids/janktube"
connstr = "postgresql://arda:123456@localhost/vidsite"
conn = psycopg2.connect(connstr)
es_client = elasticsearch.Elasticsearch([
    {"host": "localhost", "port": 9200}
])

cur = conn.cursor()
cur.execute("select path from videos")
existing = set(map(lambda x: x[0], cur.fetchall()))

os.chdir(video_path)
files = glob.glob("*.mp4") + glob.glob("*.mkv") + glob.glob("*.webm")
for filename in files:
    if filename in existing:
        continue
    # find id
    noext = os.path.splitext(os.path.basename(filename))[0]
    vid = noext[-11:]
    name = noext[:-12]

    # find duration
    duration = find_duration(filename)

    # add to db
    cur.execute(
        """insert into videos (vid, name, added, path, duration) 
            values (%s, %s, now(), %s, %s)""",
        (vid, name, filename, duration))

    # add to thumbnail queue
    cur.execute(
        """insert into thumb_queue (data) 
            values (json_build_object('vid', %s, 'path', %s))""",
        (vid, filename))

    # add to elasticsearch
    payload = {
        "name": name,
        "presenter": "",
        "event": "",
        "tags": "",
        "notes": ""
    }
    es_client.index(index="videos", id=vid, body=payload)

    conn.commit()
    print("Processed {}".format(vid))
