#!/usr/bin/env python3

import os
import subprocess
import psycopg2
import glob

video_path = "/media/arda/archive/youtube-vids/janktube"
os.chdir(video_path)

connstr = "postgresql://arda:123456@localhost/vidsite"
conn = psycopg2.connect(connstr)
cur = conn.cursor()

cur.execute("select vid from videos")
for row in cur:
    vid = row[0]
    url = f"https://youtu.be/{vid}"

    command = ["youtube-dl", "--write-auto-sub", "--skip-download", url]
    print(command)
    subprocess.call(command)

for filename in glob.glob("*.vtt"):
    parts = os.path.basename(filename).split(".")
    noext = parts[0]
    vid = noext[-11:]

    cur.execute(
        "update videos set subtitle_path = %s where vid = %s", (filename, vid))
    conn.commit()

    print(vid)
