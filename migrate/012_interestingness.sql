-- alter type e_rating rename to e_usefulness;
-- alter table videos rename column rating to usefulness;
create type e_interestingness as enum ('interesting', 'meh', 'boring');
alter table videos add column interestingness e_interestingness not null default 'meh';