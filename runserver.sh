#!/bin/sh
# Development server
env FLASK_ENV=development python3 app.py
# Gunicorn server with prod settings
# env JANKY_CONFIG=config.py gunicorn -k eventlet -w 1 -b 0.0.0.0:5000 app:app