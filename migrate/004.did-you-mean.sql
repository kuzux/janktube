create extension if not exists pg_trgm;
create table words as 
    select word from
        (select word from ts_stat('select search_key from videos')) ts
    where regexp_match(ts.word, '^[\+\-]?[0-9]+(.[0-9]*)?$') is null;
create index words_idx on words using gin(word gin_trgm_ops);