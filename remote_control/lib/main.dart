import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'package:adhara_socket_io/adhara_socket_io.dart';

// String apiRoot = "http://10.0.2.2:5000";
// String nowPlayingRoot = "http://10.0.2.2:5001";
// TODO: Persist these settings
String apiRoot = "http://192.168.43.143:5000";
String nowPlayingRoot = "http://192.168.43.143:5001";

void main() {
  runApp(MyApp());
}

String toMinuteHour(int totalSeconds) {
  int hrs = (totalSeconds / 3600).floor();
  int mins = ((totalSeconds % 3600) / 60).floor();
  int secs = (totalSeconds % 60).floor();

  String res = "";
  if (totalSeconds > 3600) {
    res += hrs.toString().padLeft(2, '0');
    res += ':';
  }
  res += mins.toString().padLeft(2, '0');
  res += ':';
  res += secs.toString().padLeft(2, '0');

  return res;
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Janktube Remote Control',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class VideoEntry {
  String title;
  String playerId;
  String videoId;

  int currTime;
  int duration;

  VideoEntry(
      this.title, this.playerId, this.videoId, this.currTime, this.duration);

  factory VideoEntry.fromJson(jsonData) {
    return VideoEntry(jsonData['title'], jsonData['id'], jsonData['vid'],
        jsonData['curr_time'].floor(), jsonData['duration'].floor());
  }

  String durationString() {
    if (this.currTime == null || this.duration == null) {
      return "";
    }
    return "${toMinuteHour(this.currTime)}/${toMinuteHour(this.duration)}";
  }

  String thumbnailUrl() {
    return "${apiRoot}/thumbs/${this.videoId}.jpg";
  }
}

class VideoPage extends StatefulWidget {
  final VideoEntry video;
  VideoPage({Key key, this.video}) : super(key: key);

  @override
  _VideoPageState createState() => _VideoPageState(this.video);
}

class _VideoPageState extends State<VideoPage> {
  VideoEntry video;
  SocketIO socket;
  SocketIOManager manager;

  _VideoPageState(this.video) {
    manager = SocketIOManager();
    _createSocket();
  }

  void _createSocket() async {
    socket = await manager.createInstance(SocketOptions(
      "${nowPlayingRoot}",
      nameSpace: "/",
      enableLogging: false,
      transports: [Transports.WEB_SOCKET, Transports.POLLING],
    ));

    socket.onConnectError((data) {
      print("connect error");
      print(data);
    });

    socket.onConnect((_) {});

    socket.on("update", (data) {
      print(data['id']);
      print(this.video.playerId);
      if (data['id'] == this.video.playerId) {
        setState(() {
          print("state update");
          print(data['curr_time']);
          print(this.video.durationString());
          this.video.currTime = data['curr_time'].floor();
        });
      }
    });

    socket.connect();
  }

  void _disconnect() async {
    await manager.clearInstance(socket);
  }

  void _sendCommand(String command) {
    final url = "${nowPlayingRoot}/control/${this.video.playerId}";
    final payload = jsonEncode(<String, String>{
      "command": command,
    });
    final headers = <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    };
    http.post(url, body: payload, headers: headers);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(this.video.title),
      ),
      body: Column(
        children: <Widget>[
          Image.network(this.video.thumbnailUrl()),
          Padding(
            padding: EdgeInsets.all(8),
            child: Text(this.video.durationString()),
          ),
          Expanded(
            child: RaisedButton(
              onPressed: () {
                _sendCommand("playpause");
              },
              child: Row(
                children: <Widget>[
                  Icon(Icons.play_arrow),
                  Text("Play/Pause"),
                ],
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  _sendCommand("rewind");
                },
                child: Icon(Icons.fast_rewind),
              ),
              RaisedButton(
                onPressed: () {
                  _sendCommand("fastforward");
                },
                child: Icon(Icons.fast_forward),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                onPressed: () {
                  _sendCommand("fullscreen");
                },
                child: Icon(Icons.fullscreen),
              ),
              RaisedButton(
                onPressed: () {
                  _sendCommand("mute");
                },
                child: Icon(Icons.volume_off),
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class OptionsDialog extends StatefulWidget {
  OptionsDialog({Key key}) : super(key: key);

  @override
  _OptionsDialogState createState() {
    return _OptionsDialogState();
  }
}

class _OptionsDialogState extends State<OptionsDialog> {
  final apiRootController = TextEditingController(text: apiRoot);
  final nowPlayingRootController = TextEditingController(text: nowPlayingRoot);

  @override
  void dispose() {
    apiRootController.dispose();
    nowPlayingRootController.dispose();
    super.dispose();
  }

  void _updateSettings() {
    apiRoot = apiRootController.text;
    nowPlayingRoot = nowPlayingRootController.text;
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(20.0),
      child: Column(
        children: <Widget>[
          TextField(
            decoration: InputDecoration(
              hintText: "API Root",
            ),
            controller: apiRootController,
          ),
          TextField(
            decoration: InputDecoration(
              hintText: "Now Playing API Root",
            ),
            controller: nowPlayingRootController,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text("OK"),
                onPressed: () {
                  _updateSettings();
                  Navigator.of(context).pop();
                },
              ),
              FlatButton(
                child: Text("Cancel"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title = "Available Players";

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<VideoEntry> videos;

  @override
  void initState() {
    this.videos = [];
    this._fetchVideos();
  }

  void _fetchVideos() async {
    final String url = "${nowPlayingRoot}/alive-players";
    print(url);
    final response = await http.get(url);
    final body = json.decode(response.body);

    setState(() {
      this.videos = body
          .map<VideoEntry>((record) => VideoEntry.fromJson(record))
          .toList();
    });
  }

  void _goToVideoPage(VideoEntry video) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => VideoPage(video: video)));
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        actions: <Widget>[
          Padding(
            padding: EdgeInsets.all(12.0),
            child: IconButton(
              onPressed: () {
                showDialog(
                    builder: (BuildContext context) {
                      return Dialog(
                        child: OptionsDialog(),
                      );
                    },
                    context: context);
              },
              icon: Icon(Icons.settings),
            ),
          )
        ],
      ),
      body: ListView.separated(
        padding: const EdgeInsets.all(12),
        itemCount: this.videos.length,
        itemBuilder: (BuildContext context, int index) {
          return Card(
            child: InkWell(
              child: Padding(
                padding: EdgeInsets.all(8),
                child: ListTile(
                  leading: Image.network(this.videos[index].thumbnailUrl()),
                  title: Text(this.videos[index].title),
                  subtitle: Text(this.videos[index].durationString()),
                ),
              ),
              onTap: () {
                _goToVideoPage(this.videos[index]);
              },
            ),
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _fetchVideos,
        child: const Icon(Icons.refresh),
      ),
    );
  }
}

// Center(
//         // Center is a layout widget. It takes a single child and positions it
//         // in the middle of the parent.
//         child: Column(
//           // Column is also a layout widget. It takes a list of children and
//           // arranges them vertically. By default, it sizes itself to fit its
//           // children horizontally, and tries to be as tall as its parent.
//           //
//           // Invoke "debug painting" (press "p" in the console, choose the
//           // "Toggle Debug Paint" action from the Flutter Inspector in Android
//           // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
//           // to see the wireframe for each widget.
//           //
//           // Column has various properties to control how it sizes itself and
//           // how it positions its children. Here we use mainAxisAlignment to
//           // center the children vertically; the main axis here is the vertical
//           // axis because Columns are vertical (the cross axis would be
//           // horizontal).
//           mainAxisAlignment: MainAxisAlignment.center,
//           children: <Widget>[
//             Text(
//               'You have pushed the button this many times:',
//             ),
//             Text(
//               '$_counter',
//               style: Theme.of(context).textTheme.headline4,
//             ),
//           ],
//         ),
//       ),
