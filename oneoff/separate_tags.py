#!/usr/bin/env python3

import sqlite3
import psycopg2

connstr = "postgresql://arda:123456@localhost/vidsite"
olddb = "/media/arda/archive/youtube-vids/janktube/vids.db"

with sqlite3.connect(olddb) as sqlite, psycopg2.connect(connstr) as psql:
    oldcur = sqlite.cursor()
    newcur = psql.cursor()

    oldcur.execute(
        """select vid 
            from videos 
            where watched is not null;
    """)
    oldrows = oldcur.fetchall()
    oldrows_str = ", ".join(map(lambda r: "'{}'".format(r[0]), oldrows))

    newcur.execute(
        """update videos 
            set tags = notes 
        where vid in ({});""".format(oldrows_str))
    newcur.execute(
        """update videos 
            set notes = '' 
        where vid in ({});""".format(oldrows_str))

    psql.commit()
