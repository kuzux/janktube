#!/usr/bin/python3

from pymediainfo import MediaInfo
import psycopg2
import glob
import sys
import os
import traceback
import time
import subprocess


def find_duration(filename):
    info = MediaInfo.parse(filename)
    raw = next(filter(lambda t: t.track_type == 'Video', info.tracks)).duration
    return round(float(raw))//1000  # return the value in seconds


def generate_thumb(filename, size=200, ss=0, dir="."):
    vo_arg = "jpeg:outdir={}".format(dir)
    mplayer_args = ["mplayer", filename, "-ss", str(ss), "-nosound",
                    "-vo", vo_arg, "-zoom", "-xy", str(size), "-frames", "1"]
    proc = subprocess.Popen(
        mplayer_args, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    retcode = proc.wait()
    if retcode == 0:
        return os.path.join(target_dir, "00000001.jpg")
    else:
        return None


connstr = "postgresql://arda:123456@localhost/vidsite"
video_path = "/media/arda/archive/youtube-vids/janktube"
conn = psycopg2.connect(connstr)
cur = conn.cursor()

while True:
    start_time = time.perf_counter()
    # locking the row right away
    cur.execute(
        """delete from thumb_queue
        where id = (
            select id
            from thumb_queue
            for update skip locked
            limit 1 )
        returning data;""")

    raw_data = cur.fetchone()
    if raw_data is None:
        try:
            time.sleep(5)
            continue
        except KeyboardInterrupt:
            answer = input("Do you really want to quit? (y/n): ")
            if answer == "y":
                sys.exit(0)
            else:
                continue

    try:
        data = raw_data[0]
        vid = data['vid']
        in_path = data['path']

        os.chdir(video_path)
        target_dir = os.path.join("thumbs", data['vid'])
        os.makedirs(target_dir, exist_ok=True)
        width = 200
        large_width = 400

        duration = find_duration(data['path'])
        num_thumbs = 200
        thumb_duration = duration / num_thumbs

        curr_time = 0
        half_time = duration / 2

        large_out_path = os.path.join(target_dir, "large.jpg")
        mplayer_out_path = generate_thumb(
            data['path'], ss=half_time, dir=target_dir, size=large_width)
        os.rename(mplayer_out_path, large_out_path)

        for i in range(num_thumbs):
            # ignoring the non-second part of the thumbnail shouldn't cause much of an issue
            out_filename = "thumb_{}.jpg".format(round(curr_time))
            out_path = os.path.join(target_dir, out_filename)
            # generate the thumbnail here
            mplayer_out_path = generate_thumb(
                data['path'], ss=curr_time, dir=target_dir, size=width)
            os.rename(mplayer_out_path, out_path)

            percentage = 100.0*(i/num_thumbs)
            print("\r{}: {:.1f}%".format(vid, percentage), end='')
            curr_time += thumb_duration

        end_time = time.perf_counter()

        print("\r{}: 100.0%".format(vid))
        print("{} - Done in {:.2f} seconds".format(in_path, end_time - start_time))
    except:
        print("Encountered an error!")
        traceback.print_exc()
        # move to failure queue
        cur.execute(
            """insert into thumb_fail_queue (data, failures)
            values (%s, 1)""", (raw_data, ))

    conn.commit()

conn.commit()
cur.close()
conn.close()
