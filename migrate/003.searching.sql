alter table videos add column search_key tsvector;
create index idx_fts_videos on videos using gin(search_key);
-- let's no longer execute this in light of migration 007.separate-description
-- update videos set search_key = (to_tsvector(coalesce(name, '')) 
--     || to_tsvector(coalesce(presenter, '')) 
--     || to_tsvector(coalesce(event, '')) 
--     || to_tsvector(coalesce(description, '')));