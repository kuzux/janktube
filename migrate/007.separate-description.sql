alter table videos add column tags text;
alter table videos rename column description to notes;
-- updating to search on tags, not notes, also making sure it is always 
-- recomputed. I might want to update the column in a scheduled task 
-- later on (i.e. never)
alter table videos drop column search_key cascade;
alter table videos add search_key tsvector not null
    generated always as (to_tsvector('english', coalesce(name, '')) 
        || to_tsvector('english', coalesce(presenter, '')) 
        || to_tsvector('english', coalesce(event, '')) 
        || to_tsvector('english', coalesce(tags, '')))
        stored;
create index idx_fts_videos on videos using gin(search_key);
-- separate tags and notes script also exists

-- we can also search on notes but separately
create index idx_fts_notes_videos on videos using gin(to_tsvector('english', notes));