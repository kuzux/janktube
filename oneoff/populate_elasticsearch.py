#!/usr/bin/env python3
import elasticsearch
import psycopg2

connstr = "postgresql://arda:123456@localhost/vidsite"
conn = psycopg2.connect(connstr)
es_client = elasticsearch.Elasticsearch([
    {"host": "localhost", "port": 9200}
])

cur = conn.cursor()
cur.execute(
    """select vid, name, presenter, event, tags, notes,
            watched is not null, duration
        from videos""")
for video in cur:
    payload = {
        "name": video[1],
        "presenter": video[2],
        "event": video[3],
        "tags": video[4],
        "notes": video[5],
        "watched": video[6],
        "duration": video[7]
    }
    es_client.index(index="videos", id=video[0], body=payload)
    print("Inserted", video[0])
