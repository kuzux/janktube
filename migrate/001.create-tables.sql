create table videos (
    vid char(11) not null primary key,
    name text not null,
    presenter text,
    event text,
    description text,
    added timestamp,
    watched timestamp
);
