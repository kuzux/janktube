alter table videos add column partial_watch_time integer not null default 0;
create table partial_watches (
    id serial primary key,
    vid char(11) not null references videos(vid),
    old_val integer,
    duration integer,
    watched timestamp
);
-- insert into partial_watches (vid, old_val, duration, watched)
--     select vid, 0, duration, watched from videos
--     where watched is not null;