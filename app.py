#!/usr/bin/env python3

import elasticsearch
import matplotlib.dates as mdate
from matplotlib import pyplot as plt
from flask.json import jsonify
import markdown as md
import jinja2 as j2
from flask import Flask, request, redirect, render_template, \
    send_from_directory, flash, Markup
import psycopg2.extras
import psycopg2.pool
from contextlib import contextmanager
import glob
import datetime as dt
import os
import re
from datetime import datetime
import json


# Large TODO: Create playlists, that could be watched partway through
# Playlists can behave essentislly like multi-part videos, I guess
# With the additional constraint that a video can only belong to at most
# one playlist.

app = Flask(__name__)
# Annoying that we require a slash not be present at the end of a url
app.url_map.strict_slashes = False
if app.env == "development":
    # local values
    app.secret_key = b'j30xp0ohe3nyqne'
    conn_str = "postgresql://arda:123456@localhost/vidsite"
    es_servers = [{"host": "localhost", "port": 9200}]
    data_dir = "/media/arda/archive/youtube-vids/janktube"
else:
    app.config.from_envvar("JANKY_CONFIG")

    app.secret_key = app.config['SECRET_KEY']
    conn_str = app.config['CONN_STR']
    es_servers = app.config['ES_SERVERS']
    data_dir = app.config['DATA_DIR']

pool = psycopg2.pool.SimpleConnectionPool(
    1, 20, conn_str, connection_factory=psycopg2.extras.LoggingConnection)
es_client = elasticsearch.Elasticsearch(es_servers)


# Copied and pasted from psycopg2 source (Only changing the superclass)
# TODO: Make sure this logs info, not debug (psycopg2 extras has debug hardcoded)
# https://github.com/psycopg/psycopg2/blob/master/lib/extras.py
class LoggingDictCursor(psycopg2.extras.DictCursor):
    def execute(self, query, vars=None):
        try:
            return super(LoggingDictCursor, self).execute(query, vars)
        finally:
            log_line = f"\033[96m{self.query.decode('utf-8')}\033[0m"
            self.connection.log(log_line, self)

    def callproc(self, procname, vars=None):
        try:
            return super(LoggingDictCursor, self).callproc(procname, vars)
        finally:
            log_line = f"\033[96m{self.query.decode('utf-8')}\033[0m"
            self.connection.log(log_line, self)


@contextmanager
def get_cursor(pool):
    conn = pool.getconn()
    conn.initialize(app.logger)
    cur = conn.cursor(cursor_factory=LoggingDictCursor)
    try:
        yield cur
    finally:
        conn.rollback()

    cur.close()
    pool.putconn(conn)


@contextmanager
def get_write_cursor(pool):
    conn = pool.getconn()
    conn.initialize(app.logger)
    cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    try:
        yield cur
        conn.commit()
    except Exception as e:
        conn.rollback()
        raise e

    cur.close()
    pool.putconn(conn)


def elasticsearch_to_result(hit):
    body = hit['_source']
    return {**body, 'vid': hit['_id']}


@app.template_filter("format_duration")
def format_duration(total_seconds):
    hrs = total_seconds // 3600
    mins = (total_seconds % 3600) // 60
    secs = total_seconds % 60
    res = ""
    if hrs > 0:
        res += f"{hrs:02}:"
    res += f"{mins:02}:{secs:02}"

    return res


@app.template_filter("render_markdown")
def render_markdown(text):
    escaped = j2.escape(text)
    converter = md.Markdown()
    result = converter.convert(escaped)
    return Markup(result)


@app.route('/')
def front_page():
    with get_cursor(pool) as cur:
        cur.execute("""
            select vid, presenter, name, duration
            from videos
            where partial_watch_time > duration*0.05 and watched is null;""")
        continue_watching = cur.fetchall()
        cw_ids = list(map(lambda t: t['vid'], continue_watching))

        cur.execute(
            """select vid, presenter, name, duration
            from videos
            where watched is null and vid <> all(%s)
            order by random() limit 10;""", (cw_ids, ))
        random_videos = cur.fetchall()

    return render_template('front.html', random_videos=random_videos, continue_watching=continue_watching)


@app.route('/videos')
def all_vids():
    with get_cursor(pool) as cur:
        cur.execute(
            """select vid, name, presenter, duration,
                watched, usefulness, interestingness
            from videos
            order by vid;""")
        vids = cur.fetchall()

    return render_template('allvids.html', videos=vids)


@app.route('/watch/<string:vid>')
def watch(vid):
    similar_query = {'query': {
        'boosting': {
            'positive': {
                'more_like_this': {
                    'fields': ['name', 'presenter', 'event', 'tags'],
                    'min_doc_freq': 1,
                    'min_term_freq': 1,
                    'max_query_terms': 10,
                    'like': {
                        '_index': 'videos',
                        '_id': vid
                    }
                }
            },
            'negative': {
                'match': {
                    'watched': True
                }
            },
            'negative_boost': 0.7
        }
    }}
    similar_response = es_client.search(index="videos", body=similar_query)
    similar = list(map(elasticsearch_to_result,
                       similar_response['hits']['hits']))

    similar_ids = list(map(lambda t: t['vid'], similar))
    similar_ids.append(vid)

    with get_cursor(pool) as cur:
        cur.execute(
            """select vid, name, duration, presenter, event, tags, notes,
                    partial_watch_time, watched is not null as is_watched,
                    subtitle_path is not null as has_subtitles
                from videos
                where vid=%s""", (vid, ))
        data = cur.fetchone()

        cur.execute("""
            select vid, name, presenter, duration
            from videos
            where watched is null and vid != all(%s)
            order by random() limit 5;
        """, (similar_ids, ))
        random = cur.fetchall()

    # map objects seem to be problematic in jinja
    return render_template('watch.html', info=data, similar=similar, random=random)


@app.route('/video/<string:vid>')
def video(vid):
    with get_cursor(pool) as cur:
        cur.execute("select path from videos where vid=%s", (vid, ))
        path = cur.fetchone()[0]

    return send_from_directory(data_dir, path)


@app.route('/edit/<string:vid>')
def edit(vid):
    with get_cursor(pool) as cur:
        cur.execute("""
            select vid, name, watched, presenter, event, tags,
                notes, usefulness, interestingness
            from videos where vid = %s""", (vid, ))
        data = cur.fetchone()

    return render_template('edit.html', info=data)


@app.route('/mark-partial/<string:vid>/<int:time>', methods=['POST'])
def mark_partial(vid, time):
    with get_write_cursor(pool) as cur:
        cur.execute("""
            select partial_watch_time, duration
            from videos
            where vid = %s""", (vid, ))
        old_value, duration = cur.fetchone()
        new_value = time

        if new_value > duration:
            return "", 400
        if new_value < old_value:
            return "", 304

        cur.execute("""
            update videos
            set partial_watch_time = %s
            where vid = %s""", (new_value, vid))

        # Do not insert negative watch times to table
        cur.execute("""
            insert into partial_watches(vid, old_val, duration, watched)
            values (%s, %s, %s, now())""", (vid, old_value, new_value-old_value))

    return "", 204


@app.route('/mark-complete/<string:vid>', methods=['POST'])
def mark(vid):
    with get_write_cursor(pool) as cur:
        cur.execute("""
            select watched, partial_watch_time, duration
            from videos
            where vid = %s""", (vid, ))
        watched, old_value, new_value = cur.fetchone()

        if watched is not None:
            return "", 304

        cur.execute("""
            update videos
            set watched = now(), partial_watch_time=%s
            where vid = %s""", (new_value, vid))
        cur.execute("""
            insert into partial_watches(vid, old_val, duration, watched)
            values (%s, %s, %s, now())""", (vid, old_value, new_value-old_value))

    payload = {"doc": {
        "watched": True,
    }}
    es_client.update(index="videos",
                     id=vid, body=payload)

    return "", 204


@app.route('/rate/<string:vid>', methods=['POST'])
def rate(vid):
    usefulness = request.form['usefulness']
    interestingness = request.form['interestingness']
    with get_write_cursor(pool) as cur:
        cur.execute("update videos set usefulness = %s, interestingness = %s where vid = %s",
                    (usefulness, interestingness, vid))

    flash('Rated video')
    return redirect('/edit/'+vid)


@app.route('/update/<string:vid>', methods=['POST'])
def update(vid):
    title = request.form['title']
    presenter = request.form['presenter']
    event = request.form['event']
    tags = request.form['tags']
    notes = request.form['notes']

    with get_write_cursor(pool) as cur:
        cur.execute("""
            update videos
            set name= %s, presenter= %s, event= %s, tags= %s, notes= %s
            where vid= %s""",
                    (title, presenter, event, tags, notes, vid))

    payload = {"doc": {
        "name": title,
        "presenter": presenter,
        "event": event,
        "tags": tags,
        "notes": notes
    }}
    # updating elasticsearch as well
    es_client.update(index="videos",
                     id=vid, body=payload)

    flash('Updated video details')
    return redirect('/edit/'+vid)


@app.route('/search', methods=['POST'])
def search():
    query = request.form['query']
    es_query = {'query': {
        'boosting': {
            'positive': {
                'query_string': {
                    'fields': ['name', 'presenter', 'event', 'tags', 'notes'],
                    'query': query,
                    'default_operator': 'AND'
                }
            },
            'negative': {
                'match': {
                    'watched': True
                }
            },
            'negative_boost': 0.7
        }
    }}
    raw_reply = es_client.search(index="videos", body=es_query)
    results = map(elasticsearch_to_result, raw_reply['hits']['hits'])

    return render_template('search.html', query=query, results=results)


@app.route('/stats')
def stats():
    with get_cursor(pool) as cur:
        cur.execute("""
            select vid, presenter, name, duration
            from videos
            where watched is not null
            order by watched desc limit 20; """)
        recently_watched = cur.fetchall()

        cur.execute("""
            select vid, presenter, name, duration, watched
            from videos
            order by added desc limit 20; """)
        recently_added = cur.fetchall()

        cur.execute("""
            select vid, name, duration, watched
            from videos
            where presenter = '???'
            limit 20; """)
        lacking_details = cur.fetchall()

        cur.execute("""
            select dates.date_str as date_str, coalesce(watches.duration, 0) as duration
            from (
                select to_char(day::date, 'YYYY-MM-DD') date_str
                from generate_series(current_date - interval '15 days',
                    current_date,
                    interval '1 day') day ) dates
                left join (
                    select to_char(date_trunc('day', watched), 'YYYY-MM-DD') as date_str,
                            sum(duration) as duration
                        from partial_watches
                        where watched >= now() - interval '15 days'
                        group by date_trunc('day', watched)) watches
                on dates.date_str = watches.date_str
                order by date_str desc;""")
        days_watched = cur.fetchall()

        cur.execute("select count(*) from videos;")
        total_count = cur.fetchone()[0]
        cur.execute("select count(*) from videos where watched is not null;")
        watched_count = cur.fetchone()[0]

        cur.execute("select sum(duration)/60 from videos;")
        total_mins = cur.fetchone()[0]
        cur.execute(
            "select sum(duration)/60 from videos where watched is not null;")
        watched_mins = cur.fetchone()[0]

    return render_template('stats.html',
                           recently_added=recently_added, lacking_details=lacking_details,
                           recently_watched=recently_watched, days_watched=days_watched,
                           total_mins=total_mins, watched_mins=watched_mins,
                           total_count=total_count, watched_count=watched_count)


@app.route('/stats/watchgraph.png')
def watch_graph():
    delta_hours = 12
    img_path = os.path.join(data_dir, "stats.png")
    # Always regenerate for now
    should_regen = True

    try:
        statres = os.stat(img_path)
        mod_time = datetime.fromtimestamp(statres.st_mtime)
        now = datetime.now()
        delta = now - mod_time
        if delta.total_seconds() >= 60 * 60 * delta_hours:
            should_regen = True
    except FileNotFoundError:
        should_regen = True

    if should_regen:
        # generate the image here. TODO: move the generation logic to a
        # background worker that is called every x hours
        with get_cursor(pool) as cur:
            cur.execute("""
                select dates.date_str as date_str, coalesce(watches.duration, 0) as duration
            from (
                select to_char(day::date, 'YYYY-MM-DD') date_str
                from generate_series(current_date - interval '15 days',
                    current_date,
                    interval '1 day') day ) dates
                left join (
                    select to_char(date_trunc('day', watched), 'YYYY-MM-DD') as date_str,
                            sum(duration)/3600.0 as duration
                        from partial_watches
                        where watched >= now() - interval '15 days'
                        group by date_trunc('day', watched)) watches
                on dates.date_str = watches.date_str
                order by date_str;""")
            values = cur.fetchall()

        xaxis = list(map(lambda t: t['date_str'], values))
        yaxis = list(map(lambda t: t['duration'], values))

        fig, ax = plt.subplots()
        ax.format_ydata = lambda x: f"{x:.2}"
        ax.grid(True)

        plt.plot(xaxis, yaxis)
        fig.autofmt_xdate()
        plt.savefig(img_path)
        plt.clf()

    return send_from_directory(data_dir, "stats.png")


@app.route('/queue-dl', methods=['POST'])
def queue_dl():
    url = request.form['url']
    with get_write_cursor(pool) as cur:
        cur.execute("""insert into dl_queue (url) values (%s)""", (url, ))

    flash("Added video to download queue")
    # TODO: I should add a download worker some time
    return redirect('/downloads')


@app.route('/thumbs/<string:vid>.json')
def list_thumbs(vid):
    expr = os.path.join(data_dir, "thumbs", vid, "thumb_*.jpg")

    paths = glob.glob(expr)

    def to_time(path):
        name = os.path.basename(path)
        m = re.match(r'^thumb_(\d+).jpg$', name)
        if m:
            return int(m.group(1))

    res = sorted(map(to_time, paths))
    return jsonify(res)


@app.route('/thumbs/<string:vid>/<string:t>.jpg')
def get_seek_thumb(vid, t):
    filename = f"thumb_{t}.jpg"
    thumb_dir = os.path.join(data_dir, "thumbs", vid)

    return send_from_directory(thumb_dir, filename)


@app.route('/thumbs/<string:vid>.jpg')
def get_main_thumb(vid):
    thumb_dir = os.path.join(data_dir, "thumbs", vid)
    return send_from_directory(thumb_dir, "large.jpg")


@app.route('/subtitle/<string:vid>')
def subtitle(vid):
    with get_cursor(pool) as cur:
        cur.execute("select subtitle_path from videos where vid=%s", (vid, ))
        path = cur.fetchone()[0]

    return send_from_directory(data_dir, path)


@app.route('/downloads')
def downloads():
    with get_cursor(pool) as cur:
        cur.execute("select url from dl_queue")
        dl_queue = cur.fetchall()
    return render_template('downloads.html', dl_queue=dl_queue)


@app.route('/favicon.ico')
def favicon():
    return send_from_directory("static/favicon", "favicon.ico")


if __name__ == '__main__':
    if app.env == "development":
        app.run(host='0.0.0.0', port=5000, debug=True)
    else:
        print("FLASK_ENV needs to be development in order to run the development server")
