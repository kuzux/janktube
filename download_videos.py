#!/usr/bin/python3

import time
import sys
import os
import traceback
import youtube_dl
import psycopg2
import elasticsearch
import glob


class StringLogger:
    def __init__(self):
        self.msgs = []

    def debug(self, msg):
        self.msgs.append(msg)

    def warning(self, msg):
        self.msgs.append(msg)

    def error(self, msg):
        self.msgs.append(msg)

    def all_messages(self):
        return "\n".join(self.msgs)


class DownloadJob:
    def __init__(self, cur, id):
        self.cur = cur
        self.id = id
        self.logger = StringLogger()

    @staticmethod
    def create(cur, url):
        cur.execute(
            "insert into dl_jobs (url) values (%s) returning id", (url, ))
        id = cur.fetchone()
        return DownloadJob(cur, id)

    def update_progress(self, d):
        if 'downloaded_bytes' in d and 'total_bytes' in d:
            progress = d['downloaded_bytes']/d['total_bytes']
        else:
            progress = 0

        if d['status'] == 'finished':
            print(f"\nDownloaded {d['filename']}")
        else:
            print(f"\rDownloading {d['filename']}, {d['_percent_str']} at {d['_speed_str']}. eta: {d['_eta_str']}",
                  end="")
        # TODO: I'll need to do this in a separate transaction
        self.cur.execute("update dl_jobs set progress=%s, status=%s where id=%s",
                         (progress, d['status'], self.id))

    def update_logs(self):
        self.cur.execute("update dl_jobs set logs=%s where id=%s",
                         (self.logger.all_messages(), self.id))


connstr = "postgresql://arda:123456@localhost/vidsite"
video_path = "/media/arda/archive/youtube-vids/janktube"
os.chdir(video_path)
conn = psycopg2.connect(connstr)
es_client = elasticsearch.Elasticsearch([
    {"host": "localhost", "port": 9200}
])

while True:
    cur = conn.cursor()

    # locking the row right away
    cur.execute(
        """delete from dl_queue
        where id = (
            select id
            from dl_queue
            for update skip locked
            limit 1 )
        returning url;""")

    raw_data = cur.fetchone()
    if raw_data is None:
        try:
            time.sleep(5)
            continue
        except KeyboardInterrupt:
            answer = input("Do you really want to quit? (y/n): ")
            if answer == "y":
                sys.exit(0)
            else:
                continue

    dl_job = None
    try:
        url = raw_data[0]
        dl_job = DownloadJob.create(cur, url)

        filename = None

        ydl_opts = {
            "format": "bestvideo[height<=720, ext=mp4]+bestaudio[ext=m4a]/best[height<=720, ext=mp4]/best",
            "writeautomaticsub": True,
            "logger": dl_job.logger,
            "progress_hooks": [dl_job.update_progress]}
        ydl = youtube_dl.YoutubeDL(ydl_opts)
        print(f"Downloading {url}")
        info = ydl.extract_info(url, download=True)
        # TODO: checlk ydl._download_retcode (or some actual public thing that lets us know the download is finished)
        ydl.process_info(info)
        filename = info['_filename']

        vid = info['id']
        duration = info['duration']
        # TODO: Do something with this
        desc = info['description']
        name = info['title']

        subs = info['requested_subtitles']
        if subs is None or len(subs) == 0:
            subtitle_path = None
        else:
            lang, sub = next(iter(subs.items()))
            subtitle_path = glob.glob(f"*{vid}.{lang}.{sub['ext']}")[0]

        cur.execute(
            """insert into thumb_queue (data)
                values (json_build_object('vid', %s, 'path', %s));""",
            (vid, filename))

        # add to elasticsearch
        payload = {
            "name": name,
            "presenter": "",
            "event": "",
            "tags": "",
            "notes": "",
            "duration": duration
        }
        es_client.index(index="videos", id=vid, body=payload)

        try:
            cur.execute(
                """insert into videos (vid, name, added, path, subtitle_path, duration)
                values (%s, %s, now(), %s, %s, %s)""",
                (vid, name, filename, subtitle_path, duration))
        except psycopg2.errors.UniqueViolation:
            print(f"{vid} - {name} Already in db")

        # print(f"Dry-running the download {url}")
    except:
        print("Encountered an error!")
        traceback.print_exc()

        message = traceback.format_exc()

        job_id = None
        if dl_job:
            job_id = dl_job.id
        # move to failure queue
        cur.execute(
            """insert into dl_fail_queue (url, message, failures, job_id)
            values (%s, %s, 1, %s)""", (url, message, job_id))

    if dl_job:
        dl_job.update_logs()
    conn.commit()
    print(f"Successfully processed {raw_data[0]}")
    cur.close()
