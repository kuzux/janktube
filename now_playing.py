from flask import Flask, request
from flask.json import jsonify
from flask_socketio import SocketIO
from datetime import datetime
import datetime as dt

app = Flask(__name__)
socketio = SocketIO(app, logger=app.logger,
                    cors_allowed_origins="*", async_mode='eventlet')

# each record is uuid -> info pair
# info has fields vid, title, last_heartbeat, curr_time, duration
now_playing = dict()


@socketio.on('start')
# data has properties id, vid, curr_time, duration
def start_video_handler(data):
    now = datetime.utcnow()
    value = dict(vid=data['vid'], duration=int(data['duration']), title=data['title'],
                 curr_time=int(data['curr_time']), last_heartbeat=now)
    if data['id'] in now_playing:
        app.logger.info(f"Video Player {data['id']} reconnecting")
    print(now_playing.keys())
    now_playing[data['id']] = value


@socketio.on('heartbeat')
# data has properties id, curr_time
def heartbeat_handler(data):
    if data['id'] in now_playing:
        now_playing[data['id']]['curr_time'] = data['curr_time']
        now_playing[data['id']]['last_heartbeat'] = datetime.utcnow()
        # sending to remote control
        socketio.emit('update', data)


@app.route('/now-playing/<string:player_id>')
# Basically get video info, but less info and in json (and has one more
# indirection)
def get_now_playing(player_id):
    details = now_playing[player_id]
    res = {'player_id': player_id, **details}

    return jsonify(res)


@app.route('/control/<string:player_id>', methods=['POST'])
# request body has a command attribute, but might contain more
def send_control(player_id):
    payload = request.json
    command = payload['command']
    del payload['command']
    if player_id in now_playing:
        socketio.emit(command, payload,
                      namespace=f"/player/{player_id}")
    return '', 204


@app.route('/alive-players')
def get_alive_players():
    now = datetime.utcnow()
    limit_time = now - dt.timedelta(seconds=30)
    res = []
    for k, v in now_playing.items():
        if v['last_heartbeat'] >= limit_time:
            payload = dict(id=k, vid=v['vid'], title=v['title'],
                           curr_time=v['curr_time'], duration=v['duration'])
            res.append(payload)

    return jsonify(res)


if __name__ == '__main__':
    if app.env == "development":
        socketio.run(app, host='0.0.0.0', port=5001,
                     debug=True, log_output=True)
    else:
        print("FLASK_ENV needs to be development in order to run the development server")
